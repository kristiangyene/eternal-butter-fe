import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import NavBar from './components/common/Navbar';
import LoginPage from './components/login/LoginPage';
import RegisterPage from './components/register/RegisterPage';
import DashboardPage from './components/dashboard/DashboardPage';
import ChatRoomPage from './components/chat/ChatRoomPage';
import ProfilePage from './components/profile/ProfilePage';
import PageNotFound from './components/notFound/PageNotFound';

function App() {
  return (
    <Router>
      <div className='App'>
        <NavBar/>
      <Switch>
        <Route exact path='/'>
          <Redirect to='/login'/>
        </Route>
        <Route path='/login' component={ LoginPage }/>
        <Route path='/register' component={ RegisterPage }/>
        <Route path='/dashboard' component={ DashboardPage }/>
        <Route path="/chatroom/:roomId" component={ ChatRoomPage }/>
        <Route path='/profile' component={ ProfilePage }/>
        <Route path='*' component={ PageNotFound }></Route>
      </Switch>
    </div>
    </Router>
  );
}

export default App;
