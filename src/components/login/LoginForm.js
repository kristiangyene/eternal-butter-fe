import React from 'react';
import './LoginForm.css';
import { Link } from 'react-router-dom';
import CC_logo from '../../assets/CC_logo.png';
import { useForm } from 'react-hook-form';

function LoginForm(props) {

    const { register, errors, handleSubmit } = useForm();
    const sendLogin = data => props.onLogin(data);


    return (
        <div className='loginForm'>
            <div className='container'>
            <form className='responsive' onSubmit={handleSubmit(sendLogin)}>
                <img className='responsive' src={CC_logo} alt="logo"/>
                <h3 style={{textAlign: 'center'}}>Log in</h3>
                <div className="form-group">
                <input
                        type="text"
                        name="username"
                        className="form-control"
                        placeholder="Username"
                        ref={register({required: true})}
                    />
                    {errors.username?.type === "required" && (
                        <p className="invalid">Username is required</p>)}
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        name="password"
                        className="form-control"
                        placeholder="Password"
                        ref={register({required: true})}
                    />
                    {errors.password?.type === "required" && (
                    <p className="invalid">Password is required</p>)}
                </div>
                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>
  
                <button type="submit" className="btn m-1 btn-dark btn-lg btn-block">Log in</button>

                <Link className="redirect" to='/register'>
                <button type="button" className="btn m-1 btn-dark btn-lg btn-block">Register</button>
                </Link>
                <Link to='/reset-password' style={{textDecoration: 'none'}}>
                    <p className="forgot-password text-left">Forgot password?</p>
                </Link>
            </form>
            </div>
        </div>
    );
}

export default LoginForm;
