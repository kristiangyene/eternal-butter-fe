import React from 'react';
import './LoginPage.css';
import LoginForm from './LoginForm';
import UserService from '../../services/UserService';
import { useHistory } from "react-router-dom";
import { setStorage } from '../../services/StorageService';


function LoginPage() {
    const history = useHistory();

  const loginUser = async (data) => {
    await UserService.login(data.username,
                          data.password)
    .then(response => {      
      setStorage("username", response.username);
      history.push("/dashboard");
    })
    .catch(error => {
      console.error(error);
    });
};

const handleOnSubmit = (data) => {
    loginUser(data);
}

    
  return (
    <div className='loginPage'>
      <LoginForm onLogin={handleOnSubmit}/>
    </div>
  );
}

export default LoginPage;
