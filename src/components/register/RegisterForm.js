import React from 'react';
import './RegisterForm.css';
import { Link } from 'react-router-dom';
import CC_logo from '../../assets/CC_logo.png';
import { useForm } from 'react-hook-form';

function RegisterForm(props) {
    

    const { register, errors, watch, handleSubmit } = useForm();
    const sendRegistration = data => props.onRegistration(data);


    return (
    <div className='registerForm'>
        <div className='container'>
            <form className='responsive' onSubmit={handleSubmit(sendRegistration)}>
                <img className='responsive' src={CC_logo} alt="logo"/>
                <h3 style={{textAlign: 'center'}}>Register</h3>
                <div className="form-group">
                    <input
                        type="text"
                        name="username"
                        className="form-control"
                        placeholder="Username"
                        ref={register({
                        required: true,
                        minLength: 2,
                        maxLength: 50,
                        pattern: /^[ÆØÅæøåa-zA-Z\s]*$/,
                        })}
                    />
                    {errors.username?.type === "required" && (
                        <p className="invalid">Username is required</p>
                    )}
                    {errors.username?.type === "minLength" && (
                        <p className="invalid">Username must be at least 2 characters</p>
                    )}
                    {errors.username?.type === "maxLength" && (
                        <p className="invalid">Username can't exceed 50 characters</p>
                    )}
                    {errors.username?.type === "pattern" && (
                        <p className="invalid">Username can only contain letters</p>
                    )}
                </div>
                <div className="form-group">
                    <input
                        type="email"
                        name="email"
                        className="form-control"
                        placeholder="Email"
                        ref={register({
                        required: true,
                        // Regex: General Email Regex (RFC 5322 Official Standard)
                        pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                        })}
                    />
                    {errors.email?.type === "required" && (
                        <p className="invalid">E-mail is required</p>
                    )}
                    {errors.email?.type === "pattern" && (
                        <p className="invalid">Please enter a valid email address</p>
                    )}
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        name="password"
                        className="form-control"
                        placeholder="Password"
                        ref={register({
                        required: true,
                        minLength: 8,
                        // Regex: Ensure string has at least one uppercase letters and at least one special character
                        pattern: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/,
                        })}
                    />
                    {errors.password?.type === "required" && (
                        <p className="invalid">Password is required</p>
                    )}
                    {errors.password?.type === "minLength" && (
                        <p className="invalid">
                    Password must be at least 8 characters long
                    </p>
                    )}
                    {errors.password?.type === "pattern" && (
                        <p className="invalid">
                        Include at least one special character and number
                        </p>
                    )}
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        name="confirmPassword"
                        className="form-control"
                        placeholder="Confirm password"
                        ref={register({
                        required: true,
                        validate: (value) =>
                        value === watch("password") || "Passwords don't match",
                        })}
                    />
                    {errors.password?.type === "required" && (
                        <p className="invalid">Please confirm password</p>
                    )}
                    {errors.confirmPassword && (
                        <p className="invalid">{errors.confirmPassword?.message}</p>
                    )}
                </div>

                <button type="submit" className="btn m-1 btn-dark btn-lg btn-block">Register</button>

                <Link to='/login' style={{textDecoration: 'none'}}>
                    <p className="forgot-password text-left">Log in?</p>
                </Link>
            </form>
        </div>
    </div>
    );
}

export default RegisterForm;
