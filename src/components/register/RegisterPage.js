import React from 'react';
import './RegisterPage.css';
import RegisterForm from "./RegisterForm";
import UserService from '../../services/UserService';
import { useHistory } from "react-router-dom";



function RegisterPage() {
    const history = useHistory();

    const postUser = async (data) => {
            await UserService.add(data.username,
                                  data.email,
                                  data.password)
            .then(response => {
              console.log(response);
            })
            .catch(error => console.error(error));
        };


    const handleOnSubmit = (data) => {
        postUser(data);
        history.push("/login");
  }


  return (
    <div className='RegisterPage'>
      <RegisterForm onRegistration={handleOnSubmit}/>
    </div>
  );
}

export default RegisterPage;
