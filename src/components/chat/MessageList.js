import React, { useEffect, useRef } from 'react';
import Message from './Message';

function MessageList({ messages = [] }) {
  const endRef = useRef(null);

  useEffect(() => {
    // Scroll to bottom every new message.
    scrollToBottom();
  }, [messages]);

  const scrollToBottom = () => {
    endRef.current.scrollIntoView({ behavior: "smooth" });
  }
    
    messages = messages.map((message, i) => {
        return (
          <Message
            key={i}
            username={message.username}
            message={message.message}
            className={`card ${
              message.ownedByCurrentUser ? "fromMe text-white bg-dark m-1 text-right" : "toMe bg-light m-1 text-left"}`}
            />
        );
      });


    return (
        <div className='MessageList pb-5'>
            { messages }
            <div ref={endRef} />
        </div>
    );
}


export default MessageList;
