import React, { useState } from 'react';
import './ChatInput.css';
import { ReactComponent as SendIcon } from "../../assets/send-icon-w.svg";


function ChatInput(props) {
  const [ input, setInput ] = useState("");


  const handleTextChange = (event) => {
    setInput(event.target.value);
  };

  const submitMessage = (event) => {
    // Prevent refreshing page.
    event.preventDefault();
    setInput("");
    props.onSend(input);
  }
    
  return (
    <form className="input-group sticky p-1" onSubmit={submitMessage}>
      <input type="text"
        className="form-control rounded-0"
        onChange={handleTextChange}
        value={input}
        placeholder="Aa"
        required />
      <button type="submit" className="btn rounded-0 bg-dark" style={{height: "38px"}}>
        <SendIcon/>
      </button>
    </form>
  );
}

export default ChatInput;
