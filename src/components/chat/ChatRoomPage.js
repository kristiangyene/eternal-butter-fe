import React, { useState, useEffect, useRef } from 'react';
import './ChatRoomPage.css';
import io from 'socket.io-client';
import MessageList from './MessageList';
import ChatInput from './ChatInput';
import { getStorage } from '../../services/StorageService';


function ChatRoomPage(props) {
  const { roomId } = props.match.params;
  const [ username, setUsername ] = useState("");
  const [ messages, setMessages ] = useState([]);
  const socket = useRef();

  useEffect(() => {
    setUsername(getStorage('username'));

    // Connect to the server    
    socket.current = io('http://localhost:4000', { query: `username=${username}` }).connect();

    // Listen for messages from the server
    socket.current.on('server:message', message => {      
      const incomingMessage = {
        ...message,
        username: message.username,
        ownedByCurrentUser: message.sender === socket.current.id
      };
      setMessages((messages) => [...messages, incomingMessage]);
    });

    return () => {
      socket.current.disconnect();
    };

  }, [username]);


  const sendHandler = (message) => {

    const outgoingMessage = {
      message,
      username: username,
      ownedByCurrentUser: true
    };

    // Emit the message to the server
    socket.current.emit('client:message', outgoingMessage);
    setMessages((messages) => [...messages, outgoingMessage]);
  }

    
  return (
    <div className='chatRoomPage'>
        <h3 style={{textAlign: 'center', color:'purple'}}>{roomId}</h3>
        <MessageList messages={messages}/>
        <ChatInput onSend={sendHandler}/>
    </div>
  );
}

export default ChatRoomPage;



