import React from 'react';

function Message(props) {
    
  return (
    <div className={props.className}>
      <div className="card-body">
      <h5 className="card-title">{props.username}</h5>
      <p className="card-text">
          {props.message}
        </p>
      </div>
    </div>
  );
}


export default Message;
