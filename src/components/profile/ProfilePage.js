import React, { useState, useEffect } from 'react';
import './ProfilePage.css';
import { getStorage } from '../../services/StorageService';
import { useHistory } from "react-router-dom";
import UserService from '../../services/UserService';



function ProfilePage() {
    const history = useHistory();
    const [isLoading,setIsLoading] = useState(true);
    const [ user, setUser ] = useState({});

    useEffect(() => {
        getUser();
        setIsLoading(false);
    }, []);

    const getUser = async () => {
        await UserService.getUser(getStorage('username'))
        .then(response => {   
            setUser(response.user);
        })
        .catch(error => {
          console.error(error);
        });
    };

    const handleOnLogout = () => {
        localStorage.clear();
        history.push("/");
    }

  
    return (
        isLoading ? 'Loading...' :
        <div className='profilePage text-center'>
            {user && (<h3>Hello {user.username}!</h3>)}
            <h3>This page is in progress.</h3>
            <button type="button" className="btn btn-dark btn-lg w-50" onClick={handleOnLogout}>Log out</button>
        </div>
    );
}

export default ProfilePage;
