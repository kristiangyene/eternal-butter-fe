import React, { useState } from 'react';
import './DashboardPage.css';
import { useHistory } from "react-router-dom";


function DashboardPage() {
  const history = useHistory();

  const [roomNumber, setRoomNumber] = useState("");

  const handleRoomNumberChange = (event) => {
    setRoomNumber(event.target.value.toUpperCase());
  };

  const handleOnSubmit = () => {
    history.push("/chatroom/" + roomNumber);
  }


  return (
    <div className='dashboardPage'>
        <div className='container'>
          <form className='responsive' onSubmit={handleOnSubmit}>
          <h3 style={{textAlign: 'center'}}>Chat room</h3>
          <div className="form-group">
            <input
              type="text"
              name="room-number"
              className="form-control text-uppercase"
              placeholder="Room ID"
              value={roomNumber}
              onChange={handleRoomNumberChange}
            />
            </div>
          <button type="button" className="redirect btn m-1 btn-dark btn-lg btn-block" onClick={handleOnSubmit}>Join</button>
        </form>
      </div>
    </div>
  );
}

export default DashboardPage;
