import axios from 'axios';

const BASE_URL = 'http://localhost:5000';

const UserService = {
  login: async (username, password) => {
    let res = await axios.post(BASE_URL + `/api/login`, { username, password })
    return res.data || {};
  },

  getAll: async () => {
    let res = await axios.get(BASE_URL + `/api/users`);
    return res.data || [];
  },

  getUser: async (username) => {
    let res = await axios.get(BASE_URL + `/api/user/${username}`);
    return res.data || {};
  },

  add: async (username, email, password) => {
    let res = await axios.post(BASE_URL + `/api/user/`, { username, email, password })
    return res.data || {};
  },

  edit: async (id, username, email, password) => {
    let res = await axios.put(BASE_URL + `/api/user/`, { id, username, email, password })
    return res.data || {};
  },

  delete: async (id) => {
    let res = await axios.delete(BASE_URL + `/api/user/${id}`);
    return res.data || [];   
  }
}

export default UserService;